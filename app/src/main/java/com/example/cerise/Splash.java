package com.example.cerise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

public class Splash extends AppCompatActivity {

    String chart = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String URL = "https://api.deezer.com/chart/23";
        Splash.TestAsyncTask testAsyncTask = new Splash.TestAsyncTask(Splash.this, URL);
        testAsyncTask.execute();
        chartPullCheck();
    }

    public void saveChart() {
        File dir = new File(this.getFilesDir(), "cerise");
        if(!dir.exists()){
            dir.mkdir();
        }
        try {
            File gpxfile = new File(dir, "chart.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(chart);
            writer.flush();
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        Intent myIntent = new Intent(Splash.this, LoginPage.class);
        Splash.this.startActivity(myIntent);
    }

    private void chartPullCheck() {
        final Handler handler = new Handler();
        final Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                if (chart != null) {
                    //Store Chart in file/Send Chart over
                    saveChart();
                   return;
                }
                handler.postDelayed(this, 3000);
            }
        };
        handler.post(runnableCode);
    }


    public class TestAsyncTask extends AsyncTask<Void, Void, String> {
        private Context mContext;
        private String mUrl;

        public TestAsyncTask(Context context, String url) {
            mContext = context;
            mUrl = url;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultString = null;
            try {
                resultString = org.apache.commons.io.IOUtils.toString(new URL(mUrl));
                chart = resultString;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return resultString;
        }

        @Override
        protected void onPostExecute(String strings) {
        }
    }

}
