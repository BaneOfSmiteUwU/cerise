package com.example.cerise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.example.cerise.util.AppUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class MainActivity extends AppCompatActivity {
    ImageButton feelingLucky[] = new ImageButton[3];
    LinearLayout trendingLL = null;
    LinearLayout recentlyLL = null;

    String luckyJson = null;
    JSONObject obj = null;
    public List<Song> trendingList = new ArrayList<Song>();
    public List<Song> recentList = new ArrayList<Song>();
    public List<Song> feelLucky = new ArrayList<Song>();
    private List<SongCollection> playlistList = new ArrayList<SongCollection>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        trendingLL = findViewById(R.id.trendingLinearLayout);
        recentlyLL = findViewById(R.id.recentlyLinearLayout);

        feelingLucky[0] = findViewById(R.id.LuckyOne);
        feelingLucky[1] = findViewById(R.id.LuckyTwo);
        feelingLucky[2] = findViewById(R.id.LuckyThree);
        loadTrending();
        loadRecentlyPlayed();

        String URL = "https://api.deezer.com/chart/23/tracks&index=10&limit=200";
        MainActivity.TestAsyncTask testAsyncTask = new MainActivity.TestAsyncTask(MainActivity.this, URL);
        testAsyncTask.execute();
        loadLucky();

    }

    public void playSong(int a, String type) {
        //Load Intent and play song
        int player = 0;
        if (type.equals("trending")) {
            AppUtil.popMessage(this, "Streaming Song: " + trendingList.get(a).getTitle());
        } else if (type.equals("recent")) {
            player = 1;
            AppUtil.popMessage(this, "Streaming Song: " + recentList.get(a).getTitle());
        }
        sendDataToActivity(a , player);
    }

    public void sendDataToActivity(int start, int playerId) {
        Intent intent = new Intent(this, PlaySong.class);
        Bundle bundle = new Bundle();
        switch (playerId) {
            case 0:
                bundle.putSerializable("playlist", (Serializable) trendingList);
                break;
            case 1:
                bundle.putSerializable("playlist", (Serializable) recentList);
                break;
            case 2:
                bundle.putSerializable("playlist", (Serializable) feelLucky);
                break;
        }
        intent.putExtras(bundle);
        intent.putExtra("id", start);
        startActivity(intent);
    }

    public void loadLikedPage(View view) {
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();

        List<Song> likedSongs = new ArrayList<Song>();

        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Liked Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (_obj.getString("collectionName").equals("LikedSongs") && _obj.getBoolean("isLikedSong")) {
                            if (_array.length() <= 0) {
                                Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                likedSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                        }
                    }
                }
                if (likedSongs == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlist", (Serializable) likedSongs);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "Liked Songs");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        } else { Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show(); }
    }

    public void loadRecentlyPlayed() {
        File recentlyPlayedFile = new File(new File(this.getFilesDir(), "cerise"), "recentlyPlayed.txt");
        String recentlyPlayedJson = null;
        if (recentlyPlayedFile.exists()) {
            try {
                Scanner in = new Scanner(recentlyPlayedFile);
                recentlyPlayedJson = in.nextLine();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                JSONArray recently = new JSONArray(recentlyPlayedJson);
                for (int i = 0; i < recently.length(); i++) {
                    recentList.add(new Song(recently.getJSONObject(i).getInt("id"), recently.getJSONObject(i).getString("title"), recently.getJSONObject(i).getString("artist"), recently.getJSONObject(i).getString("fileLink"), recently.getJSONObject(i).getString("coverArt"), 30));
                }
                generateScrollView(recentlyLL, recentList, "recent");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void goToSearch(View view) {
        startActivity(new Intent(this, SearchActivity.class));
    }

    public void loadTrending() {
        String trendingJson = null;
        File dir = new File(this.getFilesDir(), "cerise");
        try {
            Scanner in = new Scanner(new File(dir, "chart.txt"));
            trendingJson = in.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            obj = new JSONObject(trendingJson);
            JSONObject tracksObj = obj.getJSONObject("tracks");
            JSONArray tracksData = tracksObj.getJSONArray("data");
            for (int i = 0; i < tracksData.length(); i++) {
                JSONObject chart = tracksData.getJSONObject(i);
                JSONObject chartAlbum = chart.getJSONObject("album");
                String cover = chartAlbum.getString("cover_big");//Title artist, link, cover, length
                trendingList.add(new Song(chart.getInt("id"), chart.getString("title"), chart.getJSONObject("artist").getString("name"), chart.getString("preview"), chartAlbum.getString("cover_big"), 30));
            }
            generateScrollView(trendingLL, trendingList, "trending");
        } catch (Throwable t) { }
    }

    public void goToPlaylist(View view) {
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();
        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Playlists Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (!_obj.getBoolean("isLikedSong")) {
                            List<Song> playlistSongs = new ArrayList<Song>();
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                playlistSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                            playlistList.add(new SongCollection(_obj.getString("collectionName"), playlistSongs, false));
                        }
                    } if (playlistList.size() <= 0) {
                        Toast.makeText(this, "There are no Playlist!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (playlistList == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlistList", (Serializable) playlistList);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "PlayList");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        } else { Toast.makeText(this, "There are no Playlist!", Toast.LENGTH_SHORT).show(); }
    }

    private void generateScrollView(LinearLayout _ll, List<Song> _songs, final String _playType) {
        for (int i = (_songs.size()-1); i > -1; i--) {
            final int a = i;
            ImageButton _cover = new ImageButton(this);
            _cover.setId(View.generateViewId());
            Picasso.get().load(_songs.get(i).getCoverArt()).into(_cover);
            _cover.setScaleType(ImageView.ScaleType.FIT_CENTER);
            _cover.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
            _ll.addView(_cover, 0);
            _cover.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    playSong(a, _playType);
                }
            });
        }
    }

    public class TestAsyncTask extends AsyncTask<Void, Void, String> {
        private Context mContext;
        private String mUrl;

        public TestAsyncTask(Context context, String url) {
            mContext = context;
            mUrl = url;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultString = null;
            try {
                resultString = org.apache.commons.io.IOUtils.toString(new URL(mUrl));
                luckyJson = resultString;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return resultString;
        }

        @Override
        protected void onPostExecute(String strings) {
        }
    }

    private void loadLucky() {
        final Handler handler = new Handler();
        final Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                if (luckyJson != null) {
                    //Store Chart in file/Send Chart over
                    try {
                        JSONArray _array = new JSONObject(luckyJson).getJSONArray("data");
                        List<Integer> rnd = new ArrayList<Integer>();
                        for (int i = 0; i < 200; i++) {
                            rnd.add(i);
                        }
                        for (int luckyIndex = 0; luckyIndex < 3; luckyIndex++) {
                            int songNum = new Random().nextInt(rnd.size());
                            rnd.remove(songNum);
                            int id = _array.getJSONObject(songNum).getInt("id");
                            String title = _array.getJSONObject(songNum).getString("title");
                            String link =_array.getJSONObject(songNum).getString("preview");
                            String cover =_array.getJSONObject(songNum).getJSONObject("album").getString("cover_big");
                            String artist = _array.getJSONObject(songNum).getJSONObject("artist").getString("name");
                            Song _s = new Song(id, title, artist, link, cover, 30);
                            feelLucky.add(_s);
                            Picasso.get().load(cover).into(feelingLucky[luckyIndex]);
                        }
                        ((ViewManager)findViewById(R.id.loading).getParent()).removeView(findViewById(R.id.loading));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    return;
                }
                handler.postDelayed(this, 2000);
            }
        };
        handler.post(runnableCode);
    }

    public void playLuckySongs(View view) {
        int a = 0;
        switch (AppUtil.getResourceId(this, view)) {
            case "LuckyTwo":
                a = 1;
                break;
            case"LuckyThree":
                a = 2;
                break;
        }
        sendDataToActivity(a , 2);
    }

}

