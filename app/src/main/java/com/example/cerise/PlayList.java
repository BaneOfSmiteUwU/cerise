package com.example.cerise;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PlayList extends AppCompatActivity {

    LinearLayout playlistScrollView = null;
    TextView playlistName = null;
    public List<Song> playlist = new ArrayList<Song>();
    private List<SongCollection> playlistList = new ArrayList<SongCollection>();
    private List<Song> likedSongs = new ArrayList<Song>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_list);

        playlistScrollView = findViewById(R.id.ScrollViewLL);
        playlistName = findViewById(R.id.PlayListName);
        loadPlaylist();
        retrieveData();
    }

    private void retrieveData() {
        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Liked Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (_obj.getString("collectionName").equals("LikedSongs") && _obj.getBoolean("isLikedSong")) {
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                likedSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                        }
                    }
                }
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void generatePlayList() {
        if (playlistScrollView.getChildCount() > 0) {
            playlistScrollView.removeAllViewsInLayout();
        }
        for (int i = 0; i < playlistList.size(); i++) {
            final int a = i;
            final String _playListName = playlistList.get(i).getName();
            final ImageButton _kebabmenu = new ImageButton(this);
            _kebabmenu.setId(View.generateViewId());
            _kebabmenu.setScaleType(ImageView.ScaleType.FIT_CENTER);
            _kebabmenu.setImageResource(R.drawable.icon_kebab_menu);
            _kebabmenu.setLayoutParams(new LinearLayout.LayoutParams(150, 150));
            _kebabmenu.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            ImageButton _coverArt = new ImageButton(this);
            ConstraintLayout mConstraintLayout = new ConstraintLayout(this);
            mConstraintLayout.setId(View.generateViewId());
            _coverArt.setId(View.generateViewId());
            TextView _playlistName = new TextView(this);
            _playlistName.setId(View.generateViewId());
            Typeface typeface = ResourcesCompat.getFont(this, R.font.blueberry);
            _playlistName.setTypeface(typeface);
            _coverArt.setScaleType(ImageView.ScaleType.FIT_CENTER);
            List<Song> _temp = playlistList.get(i).getSong();
            if (_temp.size() != 0) {
                Picasso.get().load(_temp.get(0).getCoverArt()).into(_coverArt);
            } else {_coverArt.setImageResource(R.drawable.cerise_logo);}
            _playlistName.setText(playlistList.get(i).getName());
            mConstraintLayout.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 305));
            _coverArt.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
            ConstraintSet set = new ConstraintSet();
            mConstraintLayout.addView(_coverArt, 0);
            mConstraintLayout.addView(_playlistName, 0);
            mConstraintLayout.addView(_kebabmenu, 1);

            set.clone(mConstraintLayout);
            set.connect(_coverArt.getId(), ConstraintSet.LEFT, mConstraintLayout.getId(), ConstraintSet.LEFT, 20);
            set.connect(_playlistName.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
            set.connect(_playlistName.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 110);
            set.connect(_kebabmenu.getId(), ConstraintSet.TOP, mConstraintLayout.getId(), ConstraintSet.TOP, 0);
            set.connect(_kebabmenu.getId(), ConstraintSet.BOTTOM, mConstraintLayout.getId(), ConstraintSet.BOTTOM, 0);
            set.connect(_kebabmenu.getId(), ConstraintSet.RIGHT, mConstraintLayout.getId(), ConstraintSet.RIGHT, 10);
            _coverArt.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    expandPlayList(a, _playListName);
                }
            });
            _kebabmenu.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    deletePlayList(a, _kebabmenu);
                }
            });
            set.applyTo(mConstraintLayout);
            setNewParent(playlistScrollView, mConstraintLayout);
        }
    }

    public void loadPlaylist() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        playlistName.setText(bundle.getString("playListName"));
        if (bundle.containsKey("playlist")) {
            playlist = (List<Song>) bundle.getSerializable("playlist");
            for (int i = 0; i < playlist.size(); i++) {
                final int a = i;
                ImageButton _coverArt = new ImageButton(this);
                ConstraintLayout mConstraintLayout = new ConstraintLayout(this);
                mConstraintLayout.setId(View.generateViewId());
                _coverArt.setId(View.generateViewId());
                TextView _songTitle = new TextView(this);
                _songTitle.setId(View.generateViewId());
                TextView _artist = new TextView(this);
                _artist.setId(View.generateViewId());
                Typeface typeface = ResourcesCompat.getFont(this, R.font.blueberry);
                _artist.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                _artist.setTypeface(typeface);
                _songTitle.setTypeface(typeface);
                _coverArt.setScaleType(ImageView.ScaleType.FIT_CENTER);

                Picasso.get().load(playlist.get(i).getCoverArt()).into(_coverArt);
                _artist.setText(playlist.get(i).getArtist());
                _songTitle.setText(playlist.get(i).getTitle());
                mConstraintLayout.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 305));
                _coverArt.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
                ConstraintSet set = new ConstraintSet();
                mConstraintLayout.addView(_coverArt, 0);
                mConstraintLayout.addView(_songTitle, 0);
                mConstraintLayout.addView(_artist, 0);
                set.clone(mConstraintLayout);
                set.connect(_coverArt.getId(), ConstraintSet.LEFT, mConstraintLayout.getId(), ConstraintSet.LEFT, 20);
                set.connect(_songTitle.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
                set.connect(_songTitle.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 90);
                set.connect(_artist.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
                set.connect(_artist.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 140);
                _coverArt.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        playSong(a);
                    }
                });
                set.applyTo(mConstraintLayout);
                setNewParent(playlistScrollView, mConstraintLayout);
            }
        } else {
            playlistList = (List<SongCollection>) bundle.getSerializable("playlistList");
            generatePlayList();
        }
    }

    public void playSong(int start) {
        Intent intent = new Intent(this, PlaySong.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("playlist", (Serializable) playlist);
        intent.putExtras(bundle);
        intent.putExtra("id", start);
        startActivity(intent);
    }

    public void expandPlayList(int playListId, String _playListName) {
        playlist = playlistList.get(playListId).getSong();
        System.out.println(playlist.size());
        playlistName.setText(_playListName);
        if (playlistScrollView.getChildCount() > 0) {
            playlistScrollView.removeAllViewsInLayout();
        }
        if (playlist.size() <= 0) {
            TextView _empty = new TextView(this);
            _empty.setId(View.generateViewId());
            _empty.setTypeface(ResourcesCompat.getFont(this, R.font.blueberry));
            _empty.setText("There are no songs in this playlist!");
            _empty.setPadding(50, 0, 0, 0);
            setNewParent(playlistScrollView, _empty);
        }
        for (int i = 0; i < playlist.size(); i++) {
            final int a = i;
            ImageButton _coverArt = new ImageButton(this);
            ConstraintLayout mConstraintLayout = new ConstraintLayout(this);
            mConstraintLayout.setId(View.generateViewId());
            _coverArt.setId(View.generateViewId());
            TextView _songTitle = new TextView(this);
            _songTitle.setId(View.generateViewId());
            TextView _artist = new TextView(this);
            _artist.setId(View.generateViewId());
            final ImageButton _kebabmenu = new ImageButton(this);
            _kebabmenu.setId(View.generateViewId());
            Typeface typeface = ResourcesCompat.getFont(this, R.font.blueberry);
            _artist.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            _artist.setTypeface(typeface);
            _songTitle.setTypeface(typeface);
            _coverArt.setScaleType(ImageView.ScaleType.FIT_CENTER);
            _kebabmenu.setScaleType(ImageView.ScaleType.FIT_CENTER);
            _kebabmenu.setImageResource(R.drawable.icon_kebab_menu);
            Picasso.get().load(playlist.get(i).getCoverArt()).into(_coverArt);
            _artist.setText(playlist.get(i).getArtist());
            _songTitle.setText(playlist.get(i).getTitle());
            mConstraintLayout.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 305));
            _coverArt.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
            _kebabmenu.setLayoutParams(new LinearLayout.LayoutParams(150, 150));
            _kebabmenu.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            _coverArt.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            ConstraintSet set = new ConstraintSet();
            mConstraintLayout.addView(_coverArt, 0);
            mConstraintLayout.addView(_songTitle, 1);
            mConstraintLayout.addView(_artist, 1);
            mConstraintLayout.addView(_kebabmenu, 1);
            set.clone(mConstraintLayout);
            set.connect(_coverArt.getId(), ConstraintSet.LEFT, mConstraintLayout.getId(), ConstraintSet.LEFT, 20);
            set.connect(_songTitle.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
            set.connect(_songTitle.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 90);
            set.connect(_artist.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
            set.connect(_artist.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 140);

            set.connect(_kebabmenu.getId(), ConstraintSet.TOP, mConstraintLayout.getId(), ConstraintSet.TOP, 0);
            set.connect(_kebabmenu.getId(), ConstraintSet.BOTTOM, mConstraintLayout.getId(), ConstraintSet.BOTTOM, 0);
            set.connect(_kebabmenu.getId(), ConstraintSet.RIGHT, mConstraintLayout.getId(), ConstraintSet.RIGHT, 10);
            _coverArt.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    playSong(a);
                }
            });
            _kebabmenu.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    kebabMenu(a, _kebabmenu);
                }
            });
            set.applyTo(mConstraintLayout);
            setNewParent(playlistScrollView, mConstraintLayout);
        }
    }

    private void setNewParent(LinearLayout c, View a) {
        if (a.getParent() != null) {
            ((ViewGroup) a.getParent()).removeView(a);
        }
        c.addView(a);
    }

    public void goToSearch(View view) {
        startActivity(new Intent(this, SearchActivity.class));
    }

    public void loadHomeScreen(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void loadPlaylistPage(View view) {
        playlistList.clear();
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();
        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Playlists Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (!_obj.getBoolean("isLikedSong")) {
                            List<Song> playlistSongs = new ArrayList<Song>();
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                playlistSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                            playlistList.add(new SongCollection(_obj.getString("collectionName"), playlistSongs, false));
                        }
                    }
                    if (playlistList.size() <= 0) {
                        Toast.makeText(this, "There are no Playlist!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (playlistList == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlistList", (Serializable) playlistList);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "PlayList");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadLikedPage(View view) {
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();

        List<Song> likedSongs = new ArrayList<Song>();

        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Liked Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (_obj.getString("collectionName").equals("LikedSongs") && _obj.getBoolean("isLikedSong")) {
                            if (_array.length() <= 0) {
                                Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                likedSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                        }
                    }
                }
                if (likedSongs == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlist", (Serializable) likedSongs);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "Liked Songs");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void kebabMenu(final int a, View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.getMenu().add("Remove");
        popup.getMenu().add(isLike(a) ? "Unlike" : "Like");
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                List<Song> _t = new ArrayList<Song>();
                switch (item.getTitle().toString()) {
                    case "Remove":
                        for (int playListIndex = 0; playListIndex < playlistList.size(); playListIndex++) {
                            if (playlistList.get(playListIndex).getName().equals(playlistName.getText().toString())) {
                                _t = playlistList.get(playListIndex).getSong();
                                int x = 0;
                                for (Song rm : playlistList.get(playListIndex).getSong()) {
                                    if (rm.getArtist().equals(playlist.get(a).getArtist()) &&
                                            rm.getTitle().equals(playlist.get(a).getTitle())) {
                                        Song toRemove = _t.get(x);
                                        playlistList.get(playListIndex).setSong(_t);
                                        Toast.makeText(PlayList.this, "Removed: " + playlist.get(a).getTitle() + " from " + playlistName.getText().toString(), Toast.LENGTH_SHORT).show();
                                        _t.remove(toRemove);
                                        savePlaylist();
                                        expandPlayList(playListIndex, playlistName.getText().toString());
                                        return true;
                                    }
                                    x++;
                                }
                            }
                        }
                        break;
                    case "Unlike":
                        for (int likedSongsIndex = 0; likedSongsIndex < likedSongs.size(); likedSongsIndex++) {
                            Song x = likedSongs.get(likedSongsIndex);
                            if (x.getTitle().equals(playlist.get(a).getTitle()) && x.getArtist().equals(playlist.get(a).getArtist())) {
                                likedSongs.remove(likedSongsIndex);
                            }
                        }
                        savePlaylist();
                        break;
                    case "Like":
                        likedSongs.add(playlist.get(a));
                        savePlaylist();
                        break;
                }
                return true;
            }
        });
    }

    private void savePlaylist() {
        try {
            File playlistDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
            FileWriter writer = new FileWriter(playlistDir);
            List<SongCollection> _save = new ArrayList<SongCollection>();
            if (playlistList.size() > 0) {
                _save.addAll(playlistList);
            } else {
                List<Song> _empty = new ArrayList<Song>();
                _save.add(new SongCollection("empty", _empty, true));
            }
            _save.add(new SongCollection("LikedSongs", likedSongs, true));
            String toWrite = new Gson().toJson(_save);
            writer.write(toWrite);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean isLike(int a) {
        for (Song i : likedSongs) {
            if (playlist.get(a).getTitle().equals(i.getTitle()) && playlist.get(a).getArtist().equals(i.getArtist())) {
                return true;
            }
        }
        return false;
    }

    private void deletePlayList(final int a, View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.getMenu().add("Delete");
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                playlistList.remove(a);
                savePlaylist();
                if (playlistList.size() > 0) {
                    generatePlayList();
                } else {
                    loadHomeScreen(findViewById(R.id.HomePage));
                }
                return true;
            }
        });
    }

}
