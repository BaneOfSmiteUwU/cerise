package com.example.cerise;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SearchActivity extends AppCompatActivity {

    String searchResult = null;
    EditText searchInput = null;
    LinearLayout playlistScrollView = null;
    public List<Song> playlist = new ArrayList<Song>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchInput = findViewById(R.id.SearchBar);
        playlistScrollView = findViewById(R.id.ScrollViewLL);

        searchInput.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    searchSong(findViewById(R.id.SearchButton));
                    return true;
                }
                return false;
            }
        });

    }

    public void searchSong(View view) {
        String URL = "https://api.deezer.com/search?q=";
        URL += searchInput.getText().toString();
        SearchActivity.TestAsyncTask testAsyncTask = new SearchActivity.TestAsyncTask(SearchActivity.this, URL);
        testAsyncTask.execute();
        searchChecker();
    }

    private void searchChecker() {
        final Handler handler = new Handler();
        final Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                if (searchResult != null) {
                    //Store Chart in file/Send Chart over
                    loadSearchResult();
                    return;
                }
                handler.postDelayed(this, 1000);
            }
        };
        handler.post(runnableCode);
    }

    public void loadSearchResult() {
        if (playlistScrollView.getChildCount() > 0) {
            playlistScrollView.removeAllViewsInLayout();
            playlist.clear();
        }
        try {
            JSONObject _searchResult = new JSONObject(searchResult);
            JSONArray _data = _searchResult.getJSONArray("data");
            int _size = _data.length();
            for (int i = 0; i < _size; i++) {
                final int a = i;
                String _jsonCoverArt = _data.getJSONObject(i).getJSONObject("album").getString("cover_big");
                String _jsonArtist = _data.getJSONObject(i).getJSONObject("artist").getString("name");
                String _jsonTitle = _data.getJSONObject(i).getString("title");
                playlist.add(new Song(Integer.parseInt(_data.getJSONObject(i).getString("id")), _jsonTitle, _jsonArtist, _data.getJSONObject(i).getString("preview"), _jsonCoverArt, 30));

                ImageButton _coverArt = new ImageButton(this);
                ConstraintLayout mConstraintLayout = new ConstraintLayout(this);
                mConstraintLayout.setId(View.generateViewId());
                _coverArt.setId(View.generateViewId());
                TextView _songTitle = new TextView(this);
                _songTitle.setId(View.generateViewId());
                TextView _artist = new TextView(this);
                _artist.setId(View.generateViewId());
                Typeface typeface = ResourcesCompat.getFont(this, R.font.blueberry);
                _artist.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                _artist.setTypeface(typeface);
                _songTitle.setTypeface(typeface);
                _coverArt.setScaleType(ImageView.ScaleType.FIT_CENTER);
                Picasso.get().load(playlist.get(i).getCoverArt()).into(_coverArt);
                _artist.setText(playlist.get(i).getArtist());
                _songTitle.setText(playlist.get(i).getTitle());
                mConstraintLayout.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 305));
                _coverArt.setLayoutParams(new LinearLayout.LayoutParams(300, 300));
                _coverArt.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                ConstraintSet set = new ConstraintSet();
                mConstraintLayout.addView(_coverArt, 0);
                mConstraintLayout.addView(_songTitle, 0);
                mConstraintLayout.addView(_artist, 0);
                set.clone(mConstraintLayout);
                set.connect(_coverArt.getId(), ConstraintSet.LEFT, mConstraintLayout.getId(), ConstraintSet.LEFT, 20);
                set.connect(_songTitle.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
                set.connect(_songTitle.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 90);
                set.connect(_artist.getId(), ConstraintSet.LEFT, _coverArt.getId(), ConstraintSet.RIGHT, 10);
                set.connect(_artist.getId(), ConstraintSet.TOP, _coverArt.getId(), ConstraintSet.TOP, 140);
                _coverArt.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        playSong(a);
                    }
                });
                set.applyTo(mConstraintLayout);
                setNewParent(playlistScrollView ,mConstraintLayout);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        searchResult = null;
    }

    private void setNewParent(LinearLayout c,View a) {
        if (a.getParent() != null) {
            ((ViewGroup)a.getParent()).removeView(a);
        }
        c.addView(a);
    }

    public void playSong(int a) {
        sendDataToActivity(a);
    }

    public void sendDataToActivity(int start) {
        Intent intent = new Intent(this, PlaySong.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("playlist", (Serializable) playlist);
        intent.putExtras(bundle);
        intent.putExtra("id", start);
        startActivity(intent);
    }

    public class TestAsyncTask extends AsyncTask<Void, Void, String> {
        private Context mContext;
        private String mUrl;

        public TestAsyncTask(Context context, String url) {
            mContext = context;
            mUrl = url;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... params) {
            String resultString = null;
            try {
                resultString = org.apache.commons.io.IOUtils.toString(new URL(mUrl));
                searchResult = resultString;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return resultString;
        }

        @Override
        protected void onPostExecute(String strings) {
        }
    }

    public void loadHomeScreen(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void loadPlaylistPage(View view) {
        List<SongCollection> playlistList = new ArrayList<SongCollection>();
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();
        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Playlists Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (!_obj.getBoolean("isLikedSong")) {
                            List<Song> playlistSongs = new ArrayList<Song>();
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                playlistSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                            playlistList.add(new SongCollection(_obj.getString("collectionName"), playlistSongs, false));
                        }
                    } if (playlistList.size() <= 0) {
                        Toast.makeText(this, "There are no Playlist!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (playlistList == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlistList", (Serializable) playlistList);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "PlayList");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadLikedPage(View view) {
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();

        List<Song> likedSongs = new ArrayList<Song>();

        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Liked Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (_obj.getString("collectionName").equals("LikedSongs") && _obj.getBoolean("isLikedSong")) {
                            if (_array.length() <= 0) {
                                Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                likedSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                        }
                    }
                }
                if (likedSongs == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlist", (Serializable) likedSongs);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "Liked Songs");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
