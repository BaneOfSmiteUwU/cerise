package com.example.cerise;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SongCollection implements Serializable {
    private String collectionName;
    private List<Song> song = new ArrayList<Song>();
    private Boolean isLikedSong;

    public SongCollection(String _collectionName, List<Song> _song, Boolean _isLikedSong) {
        this.collectionName = _collectionName;
        this.song.addAll(_song);
        this.isLikedSong = _isLikedSong;
    }

    public String getName() {return collectionName;}
    public void setName(String _name) {this.collectionName = _name;}

    public List<Song> getSong() {return song;}
    public void setSong(List<Song> _song) {this.song = _song;}

    public Boolean getIsLikedSong() {return isLikedSong;}
    public void setLike(Boolean _isLikedSong) {this.isLikedSong = _isLikedSong;}
}

