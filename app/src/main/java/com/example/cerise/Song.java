package com.example.cerise;

import java.io.Serializable;

public class Song implements Serializable {
    private int id;
    private String title, artist, fileLink, coverArt;
    private Double songLength;

    public Song(int _id, String _title, String _artist, String _fileLink, String _coverArt, double _songLength) {
        this.id = _id;
        this.title = _title;
        this.artist = _artist;
        this.fileLink = _fileLink;
        this.coverArt = _coverArt;
        this.songLength = _songLength;
    }
    public int getId() {return id;}
    public void setId(int _id) {this.id = _id;}

    public String getTitle() {return title;}
    public void setTitle(String _title) {this.title = _title;}

    public String getArtist() {return artist;}
    public void setArtist(String _artist) {this.artist = _artist;}

    public String getFileLink() {return fileLink;}
    public void setFileLink(String _fileLink) {this.fileLink = _fileLink;}

    public String getCoverArt() {return coverArt;}
    public void setCoverArt(String _coverArt) {this.coverArt = _coverArt;}

    public Double getSongLength() {return songLength;}
    public void setSongLength(Double _songLength) {this.songLength = _songLength;}

}
