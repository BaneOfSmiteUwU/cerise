package com.example.cerise;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class PlaySong extends AppCompatActivity {
    private Boolean toggleLoop = true, like = false;
    private ImageButton shuffleLoop, pausePlay, likeUnlike;
    private TextView artistDisplay, titleDisplay;
    private List<Song> songList = new ArrayList<Song>();
    private List<Song> recentlyPlayed = new ArrayList<Song>();
    private List<Song> likedSongs = new ArrayList<Song>();
    private List<SongCollection> playlistList = new ArrayList<SongCollection>();
    private ImageView cover;
    private int currentlyPlayingId = 0;
    private MediaPlayer player = null;
    private int musicPosition = 0;
    public SeekBar musicTimeLine = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_song);
        shuffleLoop = findViewById(R.id.shuffleRepeat);
        pausePlay = findViewById(R.id.playPause);
        likeUnlike = findViewById(R.id.like);
        cover = findViewById(R.id.displayCover);
        artistDisplay = findViewById(R.id.ArtistName);
        titleDisplay = findViewById(R.id.SongName);
        musicTimeLine = findViewById(R.id.TimeLine);
        retrieveData();
    }

    private void retrieveData() {
        File dir = new File(new File(this.getFilesDir(), "cerise"), "recentlyPlayed.txt");
        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (dir.exists()) {
            try {
                Scanner in = new Scanner(dir);
                String _recentlyPlayed = in.nextLine();
                JSONArray _array = new JSONArray(_recentlyPlayed);
                for (int i = 0; i < _array.length(); i++) {
                    JSONObject t = _array.getJSONObject(i);
                    recentlyPlayed.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                }
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
        if (likedSongsDir.exists()) {  //Read Liked Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (_obj.getString("collectionName").equals("LikedSongs") && _obj.getBoolean("isLikedSong")) {
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                likedSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                        } else if (!_obj.getBoolean("isLikedSong")) {
                            List<Song> playlistSongs = new ArrayList<Song>();
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                playlistSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                            playlistList.add(new SongCollection(_obj.getString("collectionName"), playlistSongs, false));
                        }
                    }
                }
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        currentlyPlayingId = bundle.getInt("id");
        songList = (List<Song>) bundle.getSerializable("playlist");
        Picasso.get().load(songList.get(currentlyPlayingId).getCoverArt()).into(cover);
        if (isSongInList(songList.get(currentlyPlayingId), likedSongs)) {
            like = true;
            likeUnlike.setImageResource(R.drawable.icon_heart_fill);
        }
        artistDisplay.setText(songList.get(currentlyPlayingId).getArtist());
        titleDisplay.setText(songList.get(currentlyPlayingId).getTitle());
        preparePlayer();
        player.start();
        seekBarManager();
    }

    public void playOrPauseMusic(View view) {
        if (player == null) {
            preparePlayer();
        }
        if (!player.isPlaying()) {
            if (musicPosition > 0) {
                player.seekTo(musicPosition);
            }
            player.start();

            pausePlay.setImageResource(R.drawable.icon_pause);
        } else {
            pauseMusic();
        }

    }

    private void pauseMusic() {
        player.pause();
        musicPosition = player.getCurrentPosition();
        pausePlay.setImageResource(R.drawable.icon_play);
    }

    public void loopShuffle(View view) {
        toggleLoop = !toggleLoop;
        if (toggleLoop) {
            //Loop
            shuffleLoop.setImageResource(R.drawable.icon_repeat);
        } else {
            //Shuffle
            shuffleLoop.setImageResource(R.drawable.icon_shuffle);
        }
    }

    public void toggleLike(View view) {
        like = !like;
        if (like) {
            //Like Song
            likeUnlike.setImageResource(R.drawable.icon_heart_fill);
            if (!isSongInList(songList.get(currentlyPlayingId), likedSongs)) {
                likedSongs.add(songList.get(currentlyPlayingId));
            }
        } else {
            //Not Like ;(
            likeUnlike.setImageResource(R.drawable.icon_heart);
            if (isSongInList(songList.get(currentlyPlayingId), likedSongs)) {

                for (int i = 0; i < likedSongs.size(); i++) {
                    Song x = likedSongs.get(i);
                    if (x.getTitle().equals(songList.get(currentlyPlayingId).getTitle()) && x.getArtist().equals(songList.get(currentlyPlayingId).getArtist())) {
                        likedSongs.remove(i);
                    }
                }
            }
        }
        savePlaylist();
    }

    public void addToPlaylist(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        if (playlistList.size() > 0) {
            for (SongCollection a : playlistList) {
                popup.getMenu().add(a.getName());
            }
        }
        popup.getMenu().add("Create Playlist");
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Create Playlist")) {
                    createNewPlaylist();
                } else {
                    List<Song> _t = new ArrayList<Song>();
                    for (int a = 0; a < playlistList.size(); a++) {
                        if (playlistList.get(a).getName().equals(item.getTitle())) {
                            _t = playlistList.get(a).getSong();
                            int x = 0;
                            for (Song rm : playlistList.get(a).getSong()) {
                                if (rm.getArtist().equals(songList.get(currentlyPlayingId).getArtist()) &&
                                        rm.getTitle().equals(songList.get(currentlyPlayingId).getTitle())) {
                                    Song toRemove = _t.get(x);
                                    _t.remove(toRemove);
                                    playlistList.get(a).setSong(_t);
                                    savePlaylist();
                                    Toast.makeText(PlaySong.this, "Removed: " + songList.get(currentlyPlayingId).getTitle() + " from " + item.getTitle(), Toast.LENGTH_SHORT).show();
                                    return true;
                                }
                                x++;
                            }
                            _t.add(new Song(songList.get(currentlyPlayingId).getId(), songList.get(currentlyPlayingId).getTitle(), songList.get(currentlyPlayingId).getArtist(), songList.get(currentlyPlayingId).getFileLink(), songList.get(currentlyPlayingId).getCoverArt(), songList.get(currentlyPlayingId).getSongLength()));
                            playlistList.get(a).setSong(_t);
                            savePlaylist();
                            Toast.makeText(PlaySong.this, "Added: "+ songList.get(currentlyPlayingId).getTitle() +" to " + item.getTitle(), Toast.LENGTH_SHORT).show();
                            return true;
                        }
                    }
                }
                return true;
            }
        });
    }

    public void createNewPlaylist() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Create New Playlist");
        alert.setMessage("Name Your PlayList Here!");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                for (SongCollection a : playlistList) {
                    if (a.getName().equals(input.getText().toString())) {
                        Toast.makeText(PlaySong.this, "Error: PlayList Name Already Exist!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                List<Song> _t = new ArrayList<Song>();
                _t.add(new Song(songList.get(currentlyPlayingId).getId(), songList.get(currentlyPlayingId).getTitle(), songList.get(currentlyPlayingId).getArtist(), songList.get(currentlyPlayingId).getFileLink(), songList.get(currentlyPlayingId).getCoverArt(), songList.get(currentlyPlayingId).getSongLength()));
                playlistList.add(new SongCollection(input.getText().toString(), _t, false));
                Toast.makeText(PlaySong.this, songList.get(currentlyPlayingId).getTitle() + " successfully added into " + input.getText().toString(), Toast.LENGTH_SHORT).show();
                savePlaylist();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Toast.makeText(PlaySong.this, "Event Cancelled!", Toast.LENGTH_SHORT).show();
            }
        });

        alert.show();
    }

    public void playNext(View view) {
        if (toggleLoop) {
            if (currentlyPlayingId == (songList.size() - 1)) {
                currentlyPlayingId = 0;
            } else {
                currentlyPlayingId++;
            }
        } else {
            List<Song> _nextShuffle = new ArrayList<Song>();
            _nextShuffle.addAll(songList);
            _nextShuffle.remove(currentlyPlayingId);
            int rnd = new Random().nextInt((_nextShuffle.size()));
            int _id = _nextShuffle.get(rnd).getId();
            for (int i = 0; i < songList.size(); i++) {
                if (songList.get(i).getId() == _id) {
                    currentlyPlayingId = i;
                }
            }
        }

        if (isSongInList(songList.get(currentlyPlayingId), likedSongs)) {
            like = true;
            likeUnlike.setImageResource(R.drawable.icon_heart_fill);
        } else {
            like = false;
            likeUnlike.setImageResource(R.drawable.icon_heart);
        }
        artistDisplay.setText(songList.get(currentlyPlayingId).getArtist());
        titleDisplay.setText(songList.get(currentlyPlayingId).getTitle());
        stopActivities();
        Picasso.get().load(songList.get(currentlyPlayingId).getCoverArt()).into(cover);
        preparePlayer();
        player.start();
        seekBarManager();
    }

    public void playPrev(View view) {
        if (currentlyPlayingId == 0) {
            currentlyPlayingId = (songList.size() - 1);
        } else {
            currentlyPlayingId--;
        }
        if (isSongInList(songList.get(currentlyPlayingId), likedSongs)) {
            like = true;
            likeUnlike.setImageResource(R.drawable.icon_heart_fill);
        } else {
            like = false;
            likeUnlike.setImageResource(R.drawable.icon_heart);
        }
        artistDisplay.setText(songList.get(currentlyPlayingId).getArtist());
        titleDisplay.setText(songList.get(currentlyPlayingId).getTitle());
        Picasso.get().load(songList.get(currentlyPlayingId).getCoverArt()).into(cover);
        stopActivities();
        preparePlayer();
        player.start();
        seekBarManager();
    }

    private void preparePlayer() {
        player = new MediaPlayer();
        try {
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(songList.get(currentlyPlayingId).getFileLink());
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    playNext(findViewById(R.id.playNext));
                }
            });
            player.prepare();
            addSongToRecentlyPlayed(songList.get(currentlyPlayingId));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopActivities() {
        musicPosition = 0;
        setTitle("");
        player.stop();
        player.release();
        player = null;
        preparePlayer();
    }

    @Override
    public void onBackPressed() {
        stopActivities();
        finish();
    }

    public void addSongToRecentlyPlayed(Song a) {
        if (isSongInList(a, recentlyPlayed)) {
            for (int i = 0; i < recentlyPlayed.size(); i++) {
                Song x = recentlyPlayed.get(i);
                if (x.getTitle().equals(a.getTitle()) && x.getArtist().equals(a.getArtist())) {
                    recentlyPlayed.remove(i);
                }
            }
                /*for (Song x: recentlyPlayed) { //This doesn't work ;( Crashes, Log : https://pastebin.com/uhsTQdyG
                    if (x.getArtist().equals(a.getArtist()) && x.getTitle().equals(a.getTitle())) {
                        recentlyPlayed.remove(x);
                    }
                }*/
        } else if (recentlyPlayed.size() >= 10) {//Limit it to size 10.
            while (recentlyPlayed.size() != 9) {
                recentlyPlayed.remove(recentlyPlayed.size() - 1);
            }
        }
        recentlyPlayed.add(0, a);//0 means, first in the array.
        File dir = new File(this.getFilesDir(), "cerise");
        if (!dir.exists()) {
            dir.mkdir();
        }
        try {
            File gpxfile = new File(dir, "recentlyPlayed.txt");
            FileWriter writer = new FileWriter(gpxfile);
            String toWrite = new Gson().toJson(recentlyPlayed);
            writer.append(toWrite);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean isSongInList(Song compare, List<Song> compareList) {
        for (Song a : compareList) {
            if (a.getArtist().equals(compare.getArtist())) {
                if (a.getTitle().equals(compare.getTitle())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void goToSearch(View view) {
        stopActivities();
        startActivity(new Intent(this, SearchActivity.class));
    }

    public void loadLikedPage(View view) {
        stopActivities();
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("playlist", (Serializable) likedSongs);
        intent.putExtras(bundle);
        intent.putExtra("playListName", "Liked Songs");
        startActivity(intent);
    }

    public void loadHomeScreen(View view) {
        stopActivities();
        startActivity(new Intent(this, MainActivity.class));
    }

    private void savePlaylist() {
        try {
            File playlistDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
            FileWriter writer = new FileWriter(playlistDir);
            List<SongCollection> _save = new ArrayList<SongCollection>();
            if (playlistList.size() > 0) {
                _save.addAll(playlistList);
            } else {
                List<Song> _empty = new ArrayList<Song>();
                _save.add(new SongCollection("empty", _empty, true));
            }
            _save.add(new SongCollection("LikedSongs", likedSongs, true));
            String toWrite = new Gson().toJson(_save);
            writer.write(toWrite);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void seekBarManager() {
        musicTimeLine.setMax(player.getDuration() / 1000);
        final Handler mHandler = new Handler();
        PlaySong.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (player != null) {
                    int mCurrentPosition = player.getCurrentPosition() / 1000;
                    musicTimeLine.setProgress(mCurrentPosition);
                }
                mHandler.postDelayed(this, 100);
            }
        });
        musicTimeLine.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (player != null && fromUser) {
                    player.seekTo(progress * 1000);
                }
            }
        });
    }

    public void loadPlaylistPage(View view) {
        stopActivities();
        List<SongCollection> playlistList = new ArrayList<SongCollection>();
        Intent intent = new Intent(this, PlayList.class);
        Bundle bundle = new Bundle();
        File likedSongsDir = new File(new File(this.getFilesDir(), "cerise"), "PlayLists.txt");
        if (likedSongsDir.exists()) {  //Read Playlists Songs
            try {
                Scanner in = new Scanner(likedSongsDir);
                while (in.hasNextLine()) {
                    String _playlist = in.nextLine();
                    JSONArray _songCollectionList = new JSONArray(_playlist);
                    for (int a = 0; a < _songCollectionList.length(); a++) {
                        JSONObject _obj = _songCollectionList.getJSONObject(a);
                        JSONArray _array = _obj.getJSONArray("song");
                        if (!_obj.getBoolean("isLikedSong")) {
                            List<Song> playlistSongs = new ArrayList<Song>();
                            for (int i = 0; i < _array.length(); i++) {
                                JSONObject t = _array.getJSONObject(i);
                                playlistSongs.add(new Song(i, t.getString("title"), t.getString("artist"), t.getString("fileLink"), t.getString("coverArt"), 30));
                            }
                            playlistList.add(new SongCollection(_obj.getString("collectionName"), playlistSongs, false));
                        }
                    }
                    if (playlistList.size() <= 0) {
                        Toast.makeText(this, "There are no Playlist!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (playlistList == null) {
                    Toast.makeText(this, "There are no liked songs", Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putSerializable("playlistList", (Serializable) playlistList);
                intent.putExtras(bundle);
                intent.putExtra("playListName", "PlayList");
                startActivity(intent);
            } catch (FileNotFoundException | JSONException e) {
                e.printStackTrace();
            }
        }
    }
}