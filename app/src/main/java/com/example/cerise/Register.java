package com.example.cerise;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity {
    EditText email = null, password = null, confirmPassword = null;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.ConfirmPassword);
        mAuth = FirebaseAuth.getInstance();

    }

    public void registerBtn(View view) {
        createAccount(email.getText().toString(), password.getText().toString());
    }

    private void createAccount(String _email, String _password) {
        if (!validateForm()) {
            return;
        }
        mAuth.createUserWithEmailAndPassword(_email, _password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    Toast.makeText(Register.this, "Account Created!", Toast.LENGTH_LONG).show();
                    returnToLogin();
                } else {
                    Toast.makeText(Register.this, "Authentication failed.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private void returnToLogin() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(Register.this, LoginPage.class));
                    finish();
                } else {
                    overridePendingTransition(0, 0);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());

                }
            }
        });
    }

    private boolean validateForm() {
        boolean valid = true;

        String _email = email.getText().toString();
        if (TextUtils.isEmpty(_email)) {
            email.setError("Required.");
            valid = false;
        } else {
            email.setError(null);
        }
        String _password = password.getText().toString();
        if (TextUtils.isEmpty(_password)) {
            password.setError("Required.");
            valid = false;
        } else if (!_password.equals(confirmPassword.getText().toString())) {
            password.setError("Passwords Does not match");
            confirmPassword.setError("Passwords Does not match");
            valid = false;
        } else {
            password.setError(null);
        }
        return valid;
    }

    public void returnToLogin(View view) {
        startActivity(new Intent(this, LoginPage.class));
    }
}
